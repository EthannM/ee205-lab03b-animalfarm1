///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat catdB[MAX_SPECIES];


/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
strcpy(catdB[i].name, "Alice");
catdB[i].gender = FEMALE;
catdB[i].breed = MAIN_COON;
catdB[i].isFixed = 1;
catdB[i].weight = 12.34;
catdB[i].collar1_color = BLACK;
catdB[i].collar2_color = RED;
catdB[i].license = 12345;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
printf("Cat name = [%s]\n", catdB[i].name);
printf("gender = [%s]\n", genderType(catdB[i].gender));
printf("breed = [%s]\n", speciesName(catdB[i].breed));
printf("isFixed = [%s]\n", trueFalse(catdB[i].isFixed));
printf("weight = [%f]\n", catdB[i].weight);
printf("collar color 1 = [%s]\n",colorName(catdB[i].collar1_color));
printf("collar color 2 = [%s]\n", colorName(catdB[i].collar2_color));
printf("license = [%ld]\n", catdB[i].license);
   // Here's a clue of what one printf() might look like...
   // printf ("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
}

