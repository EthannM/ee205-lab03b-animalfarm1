///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

switch(color)
{
   case BLACK:return "Black";
   case WHITE:return "White";
   case RED:return "Red";
   case BLUE:return "Blue";
   case GREEN:return "Green";
   case PINK:return "pink";
   default: return NULL;
}
// We should never get here
 };



char* speciesName(enum CatBreeds catbreed)
{
switch(catbreed)
{
   case MAIN_COON:return "Main Coon";
   case MANX:return "Manx";
   case SHORTHAIR:return "Shorthair";
   case PERSIAN:return "Persian";
   case SPHYNX:return "Sphynx";
   default: return NULL;
}
};



char*trueFalse(int fixed)
{

switch(fixed)
{
   case  1:return "Yes";
   case  0:return "No";
   default: return NULL;
}

};




char*genderType(enum Gender type)
{

switch(type)
{
   case MALE:return "Male";
   case FEMALE:return "Female";
   default: return NULL;
}

};

